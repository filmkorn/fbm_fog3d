#pragma once
#include "BlinkTypes.h"
#include "Matrix4.h"

class Axis  
{
protected:
	Axis * parent;
	Matrix4 parentMatrix; //Axis parent world matrix
	Matrix4 matrix; //local Axis
	Matrix4 worldMatrix; //world Matrix

	void UpdateMatrix();
	void UpdateWorldMatrix();

public:
	Matrix4::TransformOrder tOrder;
	Matrix4::RotationOrder rOrder;
	float3 translate;
	float3 rotate;
	float3 scale;
	float uniform_scale;
	float3 skew;
	float3 pivot;
	int xform_order;
	int rot_order;
	bool useMatrix;
	
	void SetMatrix(Matrix4 mat);
	void SetParentMatrix(Matrix4 mat);
	void SetTransformOrder(Matrix4::TransformOrder t);
	void SetRotationOrder(Matrix4::RotationOrder r);
	Matrix4 GetWorldMatrix(){ return worldMatrix; }

	Axis(Matrix4::TransformOrder tOrder,
		Matrix4::RotationOrder rOrder,
		float3 translate = float3(0.0f, 0.0f, 0.0f),
		float3 rotate = float3(0.0f, 0.0f, 0.0f),
		float3 scale = float3(1.0f, 1.0f, 1.0f),
		float uniformScale = 1.0f,
		float3 skew = float3(0.0f, 0.0f, 0.0f),
		float3 pivot = float3(0.0f, 0.0f, 0.0f));

	Axis(float3 translate = float3(0.0f, 0.0f, 0.0f),
		float3 rotate = float3(0.0f, 0.0f, 0.0f),
		float3 scale = float3(1.0f, 1.0f, 1.0f),
		float uniformScale = 1.0f,
		float3 skew = float3(0.0f, 0.0f, 0.0f),
		float3 pivot = float3(0.0f, 0.0f, 0.0f));

	~Axis();
};