#include "Camera.h"
#include "BlinkTypes.h"
#include "BlinkMath.h"

Camera::Camera(
	float3 translate,
	float3 rotate,
	float3 scale,
	float uniform_scale,
	float3 skew,
	float3 pivot,
	float focal,
	float hAp,
	float vAp,
	float near,
	float far,
	float2 win_translate,
	float2 win_scale,
	float winRoll,
	float focalDist,
	float fstop
	) 
	: Axis(translate, rotate, scale, uniform_scale, skew, pivot),
	focal(focal),
	hAp(hAp),
	vAp(vAp),
	near(near),
	far(far),
	win_translate(win_translate),
	win_scale(win_scale),
	winRoll(winRoll),
	focalDist(focalDist),
	fstop(fstop)
{
	format( Format());
}


Camera::~Camera()
{
}

Ray Camera::GetEyeRay( float x, float y){

	// These should all be precalculated somewhere else for speed (like in _validate()):
	// Belongs into constructor?
	float fW = float(format_.width() + 1);
	float fH = float(format_.height() + 1);
	float fiW = 1.0f / fW; // to avoid divisions later
	float fiH = 1.0f / fH; // to avoid divisions later
	float fAspect = fiW /fiH;
	float cLens = fabs(hAp / focal);

	float cNear = max(near, 0.0f);
	float cFar = max(far, 0.0f);


	//Build ray
	// Build the NDC "uv" coordinate:
	float u = (float(x - format_.x()) + 0.5f)*fiW - 0.5f; // Scale to NDC
	u = (u + (win_translate.x*0.5f)) * cLens; // Apply projection
	float v = (float(y - format_.y()) + 0.5f)*fiH - 0.5f; // Scale to NDC
	v = (v + (win_translate.y*0.5f)) * cLens * fAspect; // Apply projection

	Ray ray = Ray();
	// Fill in the ray structure:
	ray.origin = worldMatrix.transform( float3(u*cNear, v*cNear, -cNear) );
	ray.direction = normalize(worldMatrix.transform( (float3(u*cFar, v*cFar, -cFar)) - ray.origin ));
	return ray;
}
