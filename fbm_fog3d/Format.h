#pragma once

//Should be replaced by DD::Image::Format.h
class Format
{
public:

	Format(int w = 640, int h = 480, double pa = 1) {
		//set(0, 0, w, h);
		x_ = 0;
		y_ = 0;
		name_ = 0;
		width_ = w;
		height_ = h;
		pa_ = pa;
	}

	const char* name_;
	int width_;
	int height_;
	double pa_;
	int x_;
	int y_;

	int width() const { return width_; }    //!< Width of image file in pixels
	void width(int v) { width_ = v; }
	int height() const { return height_; }  //!< Height of image file in pixels
	void height(int v) { height_ = v; }
	double pixel_aspect() const { return pa_; } //!< Ratio of pixel_width/pixel_height
	void pixel_aspect(double v) { pa_ = v; }
	int x() const { return x_;  }
	int y() const { return y_; }
	~Format();
};

