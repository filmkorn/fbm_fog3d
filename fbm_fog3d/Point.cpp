#include "Point.h"
#include "BlinkMath.h"
#include "BlinkTypes.h"
#include <iostream>


Point::Point(float3 orign, float intensity, int decayType)
	: Light(Ray(orign, float3(0.0f, 0.0f, 0.0f)), intensity, decayType)
{
}

float Point::GetRadiance(float3 at){
	if (decayType == 0){ return intensity; }
	//with decay:
	else {
		float distance = fabs(length(origin - at));
		return Decay(intensity, distance);
	}
}


Point::~Point()
{
}

