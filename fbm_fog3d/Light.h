#pragma once
#include "Ray.h"

class Light :
	public Ray
{

protected:
	float intensity;
	int decayType;

	float Decay(float radiance, float distance);


public:
	Light(float3 origin, float3 direction, float intensity, int decayType);
	Light(Ray ray, float intensity, int decayType);
	Light();

	~Light();

	virtual Ray GetShadowRay(float3 at);
	virtual Ray GetLightRay(float3 at); //only direction
	
	virtual float GetRadiance(float3 at) {
		return intensity;
	}

};

