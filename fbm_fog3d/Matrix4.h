#pragma once
#include "BlinkTypes.h"
#include <string>

// Copied from Nukes DD::Image::Matrix4.h
// Should be replaced by DD::Image::Matrix4

#define DD_IMAGE_MATRXI4_SIZE 16
#define DD_IMAGE_MATRXI4_BYTE_SIZE (DD_IMAGE_MATRXI4_SIZE*sizeof(float))


class Matrix4
{
public:
	enum TransformOrder {
		eSRT = 0,
		eSTR,
		eRST,
		eRTS,
		eTSR,
		eTRS
	};

	enum RotationOrder {
		eXYZ = 0,
		eXZY,
		eYXZ,
		eYZX,
		eZXY,
		eZYX
	};

	float
		a00, a10, a20, a30,
		a01, a11, a21, a31,
		a02, a12, a22, a32,
		a03, a13, a23, a33;

	Matrix4(){	
	};

	Matrix4(float a, float b, float c, float d,
		float e, float f, float g, float h,
		float i, float j, float k, float l,
		float m, float n, float o, float p) {
		a00 = a;
		a01 = b;
		a02 = c;
		a03 = d;
		a10 = e;
		a11 = f;
		a12 = g;
		a13 = h;
		a20 = i;
		a21 = j;
		a22 = k;
		a23 = l;
		a30 = m;
		a31 = n;
		a32 = o;
		a33 = p;
	}

	static const Matrix4& identity()
	{
		static const Matrix4 _identity(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
		return _identity;
	}

	void makeIdentity() {
		memcpy(&a00, &identity().a00, DD_IMAGE_MATRXI4_BYTE_SIZE);

		//a00, a11, a22, a33 = 1.0f;
		//a01, a02, a03, a10, a12, a13, a20, a21, a23, a30, a31, a32 = 0.0f;
	};

	/*!
	Same as the xyz of transform(v,1). This will transform a point
	in space but \e only if this is not a perspective matrix, meaning
	the last row is 0,0,0,1.
	*/
	float3 transform(const float3& v) const
	{
		return float3(
			a00 * v.x + a01 * v.y + a02 * v.z + a03,
			a10 * v.x + a11 * v.y + a12 * v.z + a13,
			a20 * v.x + a21 * v.y + a22 * v.z + a23
			);
	}

	/*! \fn float* Matrix4::operator[](int i)
	Return a pointer to the \e column number \a i. */
	float* operator[](int i) { return &a00 + i * 4; }

	/*! \fn const float* Matrix4::operator[](int i) const
	Return a pointer to the \e column number \a i. */
	const float* operator[](int i) const { return &a00 + i * 4; }


	/*! Does matrix multiplication. */
	Matrix4  operator *  (const Matrix4& b) const{
		Matrix4 product = Matrix4(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		
		for (int col = 0; col < 4; col++) {
			for (int row = 0; row < 4; row++) {
				// Multiply the row of A by the column of B to get the row, column of product.
				for (int inner = 0; inner < 4; inner++) {
					product[row][col] += (*this)[row][inner] * b[inner][col];
				}
			}
		}
		return product;
	};

	/*! Translate the transformation by \a x,y,z. */
	void translate(float x, float y, float z = 0.0f){
		a03 += x;
		a13 += y;
		a23 += z;
	};

	/*! Replace the contents with a rotation by angle (in radians) around
	the X axis. */
	void rotationX(float a){

		Matrix4 rotM = Matrix4();
		rotM.makeIdentity();
		rotM.a11 = cos(a);
		rotM.a21 = sin(a);
		rotM.a12 = -sin(a);
		rotM.a22 = cos(a);

		(*this) = (*this )* rotM;

	}

	/*! Replace the contents with a rotation by angle (in radians) around
	the Y axis. */
	void rotationY(float a){

		Matrix4 rotM = Matrix4();
		rotM.makeIdentity();
		rotM.a00 = cos(a);
		rotM.a02 = sin(a);
		rotM.a20 = -sin(a);
		rotM.a22 = cos(a);

		(*this) = (*this) * rotM;
	}


	/*! Replace the contents with a rotation by angle (in radians) around
	the Z axis. */
	void rotationZ(float a){

		Matrix4 rotM = Matrix4();
		rotM.makeIdentity();
		rotM.a00 = cos(a);
		rotM.a01 = -sin(a);
		rotM.a10 = sin(a);
		rotM.a11 = cos(a);

		(*this) = (*this) * rotM;
	}

	/*! Rotate the transformation by \a a radians about the vector x,y,z. */
	void rotate(RotationOrder rotOrder, float x, float y, float z)
	{
		switch (rotOrder){
		case eXYZ:
			rotationX(x);
			rotationY(y);
			rotationZ(z);
			break;
		case eXZY:
			rotationX(x);
			rotationZ(z);
			rotationY(y);
			break;
		case eYXZ:
			rotationY(y);
			rotationX(x);
			rotationZ(z);
			break;
		case eYZX:
			rotationY(y);
			rotationZ(z);
			rotationX(x);
			break;
		case eZXY:
			rotationZ(z);
			rotationX(x);
			rotationY(y);
			break;
		case eZYX:
			rotationZ(z);
			rotationY(y);
			rotationX(x);
			break;
		}
	};

	void rotate(RotationOrder a, float3 v){ return rotate(a, v.x, v.y, v.z); };

	/*! Scale the transformation by \a x,y,z. */
	void scale(float, float, float = 1){};

	/*! \fn void Matrix4::scale(const Vector3& v)
	Scale the transformation by the x,y,z of the vector
	*/
	void scale(const float3& v) { scale(v.x, v.y, v.z); }

	// probably wont implement this
	void skewVec(const float3& skew){};

	void set(TransformOrder tOrder,
		RotationOrder rOrder,
		const float3& pivot,
		const float3& translation,
		const float3& rotation,
		const float3& vScale,
		const float3& vSkew)
	{
		makeIdentity();

		// translate pivot to origin
		translate(pivot.x, pivot.y, pivot.z);

		// do the transform:
		switch (tOrder) {
		case eSRT:
			translate(translation.x, translation.y, translation.z);
			rotate(rOrder, rotation);
			skewVec(vSkew);
			scale(vScale.x, vScale.y, vScale.z);
			break;
		case eSTR:
			rotate(rOrder, rotation);
			skewVec(vSkew);
			translate(translation.x, translation.y, translation.z);
			scale(vScale.x, vScale.y, vScale.z);
			break;
		case eRST:
			translate(translation.x, translation.y, translation.z);
			scale(vScale.x, vScale.y, vScale.z);
			rotate(rOrder, rotation);
			skewVec(vSkew);
			break;
		case eRTS:
			scale(vScale.x, vScale.y, vScale.z);
			translate(translation.x, translation.y, translation.z);
			rotate(rOrder, rotation);
			skewVec(vSkew);
			break;
		case eTSR:
			rotate(rOrder, rotation);
			skewVec(vSkew);
			scale(vScale.x, vScale.y, vScale.z);
			translate(translation.x, translation.y, translation.z);
			break;
		case eTRS:
			scale(vScale.x, vScale.y, vScale.z);
			rotate(rOrder, rotation);
			skewVec(vSkew);
			translate(translation.x, translation.y, translation.z);
			break;
		}

		// translate origin back to pivot
		translate(-pivot.x, -pivot.y, -pivot.z);
	}

	~Matrix4(){};
};

