#pragma once
#include "Light.h"
class Point :
	public Light
{
public:
	Point();
	Point(float3 origin, float intensity, int decayType);
	~Point();

	float GetRadiance(float3 at);
};

