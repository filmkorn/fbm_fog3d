#include "Light.h"
#include "BlinkTypes.h"


Light::Light(float3 origin, float3 direction, float intensity, int decayType)
	: Ray(origin, direction),
	intensity(intensity), decayType(decayType)
{
}

Light::Light(Ray ray, float intensity, int decayType)
	: Ray(ray.origin, ray.direction), intensity(intensity), decayType(decayType)
{
}

Light::Light()
	: Ray(),
	intensity(0.0f),decayType(0)
{
}

Light::~Light()
{
}

float Light::Decay(float radiance, float distance){

	switch (decayType){	
	case 0: //no falloff
		return radiance;		
	case 1: // linear falloff:
		return radiance / distance;
	case 2: //quadratic falloff: Inverse-square law
		return radiance / pow(distance, 2);
	case 3: //cubic falloff:
		return radiance / pow(distance, 3);
	default:
		return radiance;
	}
}

Ray Light::GetShadowRay(float3 at){
	return Ray(origin,  origin - origin);
}

//Applies to spot and point light only.
// Must be overwritten by dir light.
Ray Light::GetLightRay(float3 at){
	return Ray(at, at - origin);
}
