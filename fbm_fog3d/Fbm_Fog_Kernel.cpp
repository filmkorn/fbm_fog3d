#include "Fbm_Raymarcher.h"
#include "Point.h"
#include "Spot.h"
#include "Direct.h"
#include "Ray.h"
#include "Fbm_Math.h"
#include "BlinkMath.h"
#include "BlinkTypes.h"


kernel fbm : ImageComputationKernel < ePixelWise >
{
	Image<eRead, eAccessRandom> position; //Position Pass
	Image<eWrite, eAccessPoint> dst;

param:
	float octaves;
	float gain;
	float lacunarity;
	float4 amplitude;
	float frequency;
	float Z;
	float3 offset;
	float3 scale;
	float3 cameraPosition;
	float maxZdepth;
	bool useGround;
	float ground;
	float height;
	int interpolation;
	float gamma;
	float threshold;
	int rayDepth;
	bool enableLight;
	bool enableShadows;
	int lightType;
	int lightSamples;
	float3 lightPosition;
	float3 lightDir;
	float4 lightColor;
	float lightIntensity;
	int lightDecay;
	float coneAngle;
	float conePenumbraAngle;
	float coneFalloff;
	float lightOctaves;
	float absorbance;
	float anisotropy;

local:
	int m_width;
	int m_height;

	void define()
	{
		defineParam(octaves, "Octaves", 6.0f);
		defineParam(gain, "Gain", 0.5f);
		defineParam(lacunarity, "Lacunarity", 4.0f);
		defineParam(gamma, "Gamma", 0.1f);
		defineParam(amplitude, "amplitude", float4(0.5f, 0.5f, 0.5f, 1.0f));
		defineParam(frequency, "Frequency", 1.0f);
		defineParam(Z, "Z", 0.0f);
		defineParam(offset, "Offset", float3(0.0f, 0.0f, 0.0f));
		defineParam(scale, "Scale", float3(1.0f, 1.0f, 1.0f));
		defineParam(maxZdepth, "Max zDepth", 1000.0f);
		defineParam(rayDepth, "Ray Depth", 128);
		defineParam(useGround, "Ground Fog", bool(true));
		defineParam(ground, "Ground", 0.0f);
		defineParam(height, "Height", 100.0f);
		defineParam(interpolation, "Interpolation", 0),
		defineParam(threshold, "Threshold", 0.5f);
		defineParam(enableLight, "Enable Lighting", bool(false));
		defineParam(enableShadows, "Enable Shadows", bool(false));
		defineParam(lightSamples, "Light Samples", 16);
		defineParam(lightColor, "Light Color", float4(1.0f, 1.0f, 1.0f, 1.0f)); //float4 more easy to multiply with amplitude although only float3 needed.
		defineParam(lightIntensity, "Intensity", 1.0f);
		defineParam(lightDecay, "Decay", 1); //0: no decay, 1: linear falloff, 2: quadratic falloff
		defineParam(lightOctaves, "Light Octaves", 1.0f);
		defineParam(absorbance, "Absorbance", 0.05f);
		defineParam(anisotropy, "Anisotropy", 0.0f);
	}

	void init()
	{
		m_width = dst.bounds.width();
		m_height = dst.bounds.height();
	}

	void process(int2 pos)
	{
		float4 positionhere = position(pos.x, pos.y);
		float3 position3 = float3(positionhere.x, positionhere.y, positionhere.z);
		Light light;
		Fbm_Fog fog = Fbm_Fog( octaves, gain,lacunarity,frequency,Z,scale,offset,useGround,ground,height,interpolationType,gamma,threshold)

		if (enableLight){
			amplitude.x *= lightColor.x;
			amplitude.y *= lightColor.y;
			amplitude.z *= lightColor.z;

			// Point light
			if (lightType == 0)
			{
				light = Point(lightPosition, lightIntensity, lightDecay)
			}
			// Direct light
			if (lightType == 1)
			{
				light = Direct(lightDir, lightIntensity);
			}
			// Spot light
			if (lightType == 2)
			{
				light = Spot(lightPosition, lightDir, lightIntensity, lightDecay, coneAngle, conePenumbraAngle, coneFalloff);
					)
			}
		}

		float total;

		total = raymarch(fog,
			cameraPosition,
			position3,
			maxZdepth,
			gamma,
			threshold,
			rayDepth,
			enableLight,
			light,
			absorbance,
			anisotropy
			);

		SampleType(dst) sample(0.0f);

		sample.x = total*amplitude.x;
		sample.y = total*amplitude.y;
		sample.z = total*amplitude.z;
		sample.w = 0.0f;
		//sample.w = clamp(total*amplitude.w, 0.0f, 1.0f);

		dst() = sample;
	}
};
