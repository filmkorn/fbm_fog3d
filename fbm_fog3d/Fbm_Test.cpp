#include <iostream>
#include <algorithm>
#include <vector>
#include "Point.h"
#include "Spot.h"
#include "Direct.h"
#include "Ray.h"
#include "Fbm_Raymarcher.h"
#include "BlinkMath.h"
#include "BlinkTypes.h"
#include "Fbm_Fog.h"
#include "Format.h"
#include "Camera.h"
#include <omp.h>
#include <thread>
#include <time.h>
#include <vector>

#include <OpenImageIO/imagebuf.h>

OIIO_NAMESPACE_USING

// @todo: move random offset of origin to sampler - no
// @todo: sampleCount output.
// @todo: move absorbance and phasefunction to Fbm_Fog
// @todo: verify Mersenne twister
// @todo: verify phasefunction
// @todo: correct render equation and trasmission function
// @todo: decoupled raymarching
// @todo: equiangular sampling
// @todo: ground normals
// @todo: optimization - translate to blink node
// @todo: shadowing from geo
//
// @idea: OpenVDB reader
// @idea: Volume shader for nuke scanline renderer.


int main(int argc, const char* argv[])
{
	//Noise parameter
	float octaves = 2.0f;
	float gain = 0.5f;
	float lacunarity = 3.0f;
	float frequency = 0.3f;
	float Z = 0.0f;
	float3 scale = float3(1.0f, 1.0f, 1.0f);
	float3 offset = float3(0.0f, 0.0f, 0.0f);

	//Fog parameter
	bool useGround = true;
	float ground = 0.0f;
	float height = 7.0f;
	int interpolation = 1;
	float gamma = 0.1f;
	float threshold = 0.5f;
	float absorbance = 0.01f;
	float anisotropy = 0.0f;


	//Light Parameter
	bool enableLight = true;
	bool enableShadows = false;
	int lightType = 2;
	int lightSamples = 16;
	float3 lightPosition = float3(0.0f, 5.0f, 0.0f);
	float3 lightDir = float3(0.0f, -1.0f, 0.0f);
	float3 lightColor = float3(1.0f, 1.0f, 1.0f);
	float lightIntensity = 20.0f;
	int lightDecay = 3;
	float coneAngle = 30.0f;
	float conePenumbraAngle = 10.0f;
	float coneFalloff = 0.5f;
	float lightOctaves = 1.0f;

	//Color
	float4 amplitude = float4(1.0f, 1.0f, 1.0f, 1.0f);

	//Format
	int i_width = 420;
	int i_height = 280;
	Format format = Format(i_width, i_height);

	//Camera
	Camera cam = Camera(float3(-4.0f, 10.0f, 40.0f), float3(degree2rad(-10.0f), 0.0f , 0.0f ));
	//std::cout << cam.GetWorldMatrix() << std::endl;
	cam.format(format);

	//ImageBuf
	ImageSpec spec(i_width, i_height, 4, TypeDesc::FLOAT);
	ImageBuf imageBuffer = ImageBuf("RGBA", spec);
	int nc = imageBuffer.nchannels();
	
	// Render Settings
	int samples = 2;
	samples = pow(2, samples);
	int bucketsize = 64;
	int buckets_x = std::ceil( (float)i_width / (float)bucketsize );
	int buckets_y = std::ceil( (float)i_height / (float)bucketsize );
	char output[] = "D:/Programming/Fbm_Fog3D/render/test_0030.exr";

	//Raymarcher parameter
	float maxZdepth = 50.0f;
	int steps = 64;

	//Create Lights:
	std::vector<Light*> sceneLights;
	Fbm_Fog fog = Fbm_Fog(octaves, gain, lacunarity, frequency, Z, scale, offset, useGround, ground, height, interpolation, gamma, threshold);

	if (enableLight){
		amplitude.x *= lightColor.x;
		amplitude.y *= lightColor.y;
		amplitude.z *= lightColor.z;

		
		// Point light
		if (lightType == 0)
		{
			Light* light = new Point(lightPosition, lightIntensity, lightDecay);
			sceneLights.push_back(light);
			//std::cout << (*light).GetWorldMatrix() << std::endl;
		}
		// Direct light
		if (lightType == 1)
		{
			Light* light = new Direct(lightDir, lightIntensity);
			sceneLights.push_back(light);
		}
		// Spot light
		if (lightType == 2)
		{
			Light* light = new Spot(lightPosition, lightDir, lightIntensity, lightDecay, coneAngle, conePenumbraAngle, coneFalloff);
			sceneLights.push_back(light);
			Light* light2 = new Point(float3( -8.0f, 5.0f, 0.0f), lightIntensity, lightDecay);
			sceneLights.push_back(light2);
		}
	}


	//Render
	std::cout << "Rendering... " << std::endl;
	clock_t c_start, c_end;
	c_start = clock();
	
	// for every line of buckets
	int bX , bY;
	for (bY = 0; bY < buckets_y; bY++){

		ROI bucket;
		int x_start, x_end, y_start, y_end;

		y_start = bY * bucketsize ;
		y_end = std::min(y_start + bucketsize, i_height);

		// for every bucket in the line
		#pragma omp parallel for private(bucket)
		for (bX = 0; bX < buckets_x; bX++){
			x_start = bX * bucketsize;
			x_end = std::min(x_start + bucketsize, i_width);
			bucket = ROI(x_start, x_end, y_start, y_end);

			//for every pixel in bucket
			for (ImageBuf::Iterator<float> it(imageBuffer, bucket); !it.done(); ++it) {
				Ray eyeRay;
				eyeRay = cam.GetEyeRay( it.x(), i_height  - it.y() + 1);
				eyeRay.direction = eyeRay.direction * maxZdepth;

				//#define DIAGNOSE
				#ifndef DIAGNOSE 
				float4 total{ 0.0f };
				
				//MT Sampling
				int s = 0;
				for (s = 0; s < samples; s++)
				{
					total = total + raymarch(fog, eyeRay, steps, maxZdepth, gamma, threshold, enableLight, sceneLights, absorbance, anisotropy) / (float)samples;
				}
				it[0] = total.x;
				it[1] = total.y;
				it[2] = total.z;
				it[3] = total.w;
				#endif

				#ifdef DIAGNOSE
				//it[0] = eyeRay.direction.x;
				//it[1] = eyeRay.direction.y;
				//it[2] = eyeRay.direction.z;
				it[0] = eyeRay.origin.x;
				it[1] = eyeRay.origin.y;
				it[2] = eyeRay.origin.z;
				#endif

			}
		}
		std::cout << (int)std::ceil((float)(bY + 1) / (float)buckets_y * 100.0f) << "% done..." <<  std::endl;
	}	

	c_end = clock();
	double rendertime = (double)(c_end - c_start) / CLOCKS_PER_SEC;
	std::cout << "Rendering took " << rendertime << "sec." << std::endl;
	std::cout << "Writing image" << std::endl;
	
	//Adding some metadata:
	imageBuffer.specmod().attribute("render/rendertime", (float)rendertime);
	imageBuffer.specmod().attribute("render/samples", samples);
	imageBuffer.specmod().attribute("render/steps", steps);

	imageBuffer.write( output );
	//std::cin.get();
	return 0;
}