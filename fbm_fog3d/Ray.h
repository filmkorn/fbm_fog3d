#pragma once
#include "BlinkTypes.h"
#include "Axis.h"

class Ray : public Axis
{

public:
	float3 origin;
	float3 direction;

	Ray(float3 origin, float3 direction);
	Ray();
	~Ray();
};

