#include "Axis.h"

Axis::Axis(Matrix4::TransformOrder tOrder,
	Matrix4::RotationOrder rOrder ,
	float3 translate,
	float3 rotate,
	float3 scale,
	float uniform_scale,
	float3 skew,
	float3 pivot) :
	tOrder(tOrder),
	rOrder(rOrder),
	translate(translate),
	rotate(rotate ),
	scale(scale),
	uniform_scale(uniform_scale),
	skew(skew),
	pivot(pivot)
{
	parentMatrix = Matrix4();
	parentMatrix.makeIdentity();

	matrix = Matrix4();
	matrix.makeIdentity();

	worldMatrix = Matrix4();
	worldMatrix.makeIdentity();

	UpdateMatrix();
	UpdateWorldMatrix();
};

Axis::Axis(	float3 translate,
	float3 rotate,
	float3 scale,
	float uniform_scale,
	float3 skew,
	float3 pivot) :
	translate(translate),
	rotate(rotate),
	scale(scale),
	uniform_scale(uniform_scale),
	skew(skew),
	pivot(pivot)
{
	tOrder = Matrix4::eTRS;
	rOrder = Matrix4::eXYZ;

	parentMatrix = Matrix4();
	parentMatrix.makeIdentity();

	matrix = Matrix4();
	matrix.makeIdentity();


	worldMatrix = Matrix4();
	worldMatrix.makeIdentity();

	UpdateMatrix();
	UpdateWorldMatrix();
};

// TODO: Scaling, skew, pivot
void Axis::UpdateMatrix(){
	if (!useMatrix){
		matrix.set(tOrder, rOrder, pivot, translate, rotate, scale, skew);
		Axis::UpdateWorldMatrix();
	}
}

void Axis::UpdateWorldMatrix(){
	worldMatrix = parentMatrix * matrix;
}

// this will enable useMatrix
void Axis::SetMatrix(Matrix4 mat){
	matrix = mat;
	UpdateWorldMatrix();
}

void Axis::SetTransformOrder(Matrix4::TransformOrder t){
	tOrder = t;
	matrix.set(t, rOrder, pivot, translate, rotate, scale, skew);
	Axis::UpdateWorldMatrix();
}

void Axis::SetRotationOrder(Matrix4::RotationOrder r){
	rOrder = r;
	matrix.set(tOrder, r, pivot, translate, rotate, scale, skew);
	Axis::UpdateWorldMatrix();
}

void Axis::SetParentMatrix(Matrix4 p_mat){
	parentMatrix = p_mat;
	Axis::UpdateWorldMatrix();
}

Axis::~Axis()
{
}
