#include "Direct.h"
#include "BlinkTypes.h"

Direct::Direct()
{
}

Direct::Direct(float3 direction, float intensity)
	: Light(float3(0.0f, 0.0f, 0.0f), direction, intensity, 0)
{
}

Direct::~Direct()
{
}

Ray Direct::GetLightRay(float3 at){
	return Ray(origin, direction);
}
