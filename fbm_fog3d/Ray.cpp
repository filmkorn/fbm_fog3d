#include "Ray.h"
#include "BlinkTypes.h"


Ray::Ray(){
	origin = direction = float3(0.0f, 0.0f, 0.0f);
}

Ray::Ray(float3 origin, float3 direction) 
	: Axis(origin), origin(origin), direction(direction)
{
}


Ray::~Ray()
{
}
