#include "BlinkTypes.h"
#include "BlinkMath.h"

#include "Fbm_Fog.h"
#include "Fbm_Math.h"
#include "Light.h"
#include "Spot.h"
#include "Point.h"
#include "Direct.h"
#include <vector>


float4 raymarch(
	Fbm_Fog Fog,
	Ray ray,
	int steps,
	float maxZdepth,
	float gamma,
	float threshold,
	bool enableLight,
	const std::vector<Light*> sceneLights,
	float absorbance,
	float anisotropy );

float4 raymarch(
	Fbm_Fog Fog,
	float3 start,
	float3 end,
	int steps,
	float maxZdepth,
	float gamma,
	float threshold,
	bool enableLight,
	const std::vector<Light*> sceneLights,
	float absorbance,
	float anisotropy
	);
