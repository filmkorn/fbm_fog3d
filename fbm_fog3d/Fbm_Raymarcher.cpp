#include "Fbm_Raymarcher.h"
#include <vector>
#include <random>
#include <iostream>
#include <ctime>

float4 raymarch(
	Fbm_Fog Fog,
	Ray ray,
	int steps,
	float maxlength,
	float gamma,
	float threshold,
	bool enableLight,
	const std::vector<Light*> sceneLights,
	float absorbance,
	float anisotropy
	){

	float rayLength = length(ray.direction);
	float3 samplePos;
	float stepSize = rayLength / steps;
	float3 step =  (ray.direction / rayLength ) * stepSize;


	float4 total = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 sample_total = float4(0.0f, 0.0f, 0.0f, 0.0f);
	
	float density = 1.0f; //the texture
	float radiance = 1.0f; //the light intensity at samplePos
	Ray lightRay;
	Ray shadowRay;
	//float3 lightSamplePos;

	float lowLightThreshold = 0.0001f;

	//mersenne twister:
	std::uniform_real_distribution<double> dis(0.0, 1.0);
	std::mt19937 mt_rand(rand());
	
	//randomly offsetted origin to reduce aliasing
	float3 random_offset = step * dis(mt_rand);
	float3 origin_offset = ray.origin;
	origin_offset = ray.origin + random_offset;

	float4 ray_total = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// raymarching:
	int i = 0;
	for (i = 0; i < steps; i++)
	{
		samplePos = origin_offset + scaleVect(step, (float)i);
		//return float4(samplePos.x, samplePos.y, samplePos.z, 0.0f);

		if (samplePos.y < 0.0f){
			continue;
		}


		if (enableLight) {
			radiance = 0.0f;
			for (std::vector<int>::size_type l = 0; l != sceneLights.size(); l++) {
				Light* light = sceneLights[l];
				float radiance_l{ 0.0f };
				radiance_l = (*light).GetRadiance(samplePos);
				lightRay = (*light).GetLightRay(samplePos);
				shadowRay = (*light).GetShadowRay(samplePos);


				// phase function
				// TODO: belongs into Fbm_Fog since its a fog property.				
				radiance += radiance_l * phaseFunction((angleBetween(lightRay.direction, ray.direction)), anisotropy);
			}
		}

		// low light threshold:
		if (radiance > lowLightThreshold){
			density = Fog.GetDensity(samplePos);
		}

		sample_total = density*radiance ;
		sample_total.w = density;
		//std::cout << sample_total << std::endl;
		ray_total = ray_total + sample_total;
	}
	total = total + ray_total;

	float zBias = 1.0f / (float)steps;
	total = total * zBias;
	return total;
};



float4 raymarch(
	Fbm_Fog Fog,
	float3 start,
	float3 end,
	int steps,
	float maxlength,
	float gamma,
	float threshold,
	bool enableLight,
	const std::vector<Light*> sceneLights,
	float absorbance,
	float anisotropy
	)
{
	return raymarch(
		Fog,
		Ray(start, end),
		steps,
		maxlength,
		gamma,
		threshold,
		enableLight,
		sceneLights,
		absorbance,
		anisotropy
		);
}