#include "BlinkMath.h"
#include "BlinkTypes.h"


inline float interpolate2d(float2 p1, float2 p2, float x, int type) {

	float linear = p1.y + (p2.y - p1.y) * (x - p1.x) / (p2.x - p1.x);
	if (type == 0){ return linear; } //linear
	else {
		linear = clamp(linear, 0.0f, 1.0f);
		if (type == 1) { return pow(linear, 3.0f); } //plinear
		else if (type == 2) { return linear * linear * (3.0f - 2.0f * linear); } //smooth: traditional smoothstep
		else if (type == 3) { return linear*linear *(2.0f - linear); } //smooth0: Catmull-Rom spline, smooth start, linear end
		else if (type == 4) { return linear * (1.0f + linear * (1.0f - linear)); } //smooth1: Catmull Rom spline, linear start, smooth end
		else { return linear; }
	}
};


// angle between two vectors in radians. 
// Multiply by  180.0f/M_PI to get degrees.
float angleBetween(float3 v1, float3 v2)
{
	float mag = length(cross(v1, v2));
	return atan2(mag, dot(v1, v2));
};



float3 scaleVect(float3 v, float s)
{
	return float3(v.x * s, v.y*s, v.z*s);
};