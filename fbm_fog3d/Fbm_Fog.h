#pragma once
#include "Fbm_Noise4D.h"

class Fbm_Fog
{
private:
	float octaves;
	float gain;
	float lacunarity;
	float frequency;
	float Z;
	float3 scale;
	float3 offset;
	bool useGround;
	float ground;
	float height;
	int interpolationType;
	float gamma;
	float threshold;

	Fbm_Noise4D Noise;

	float HeightFade(float ground,
		float height,
		float y,
		int interpolation);

public:
	Fbm_Fog();
	Fbm_Fog(float octaves,
		float gain,
		float lacunarity,
		float frequency,
		float Z,
		float3 scale,
		float3 offset,
		bool useGround,
		float ground,
		float height,
		int interpolationType,
		float gamma,
		float threshold);

	float GetDensity(float3 samplePos);

	~Fbm_Fog();
};


// Heney Greenstein Anisotropy (normalised):
// angle: degrees between -180 and 180
// anisotropy = 0: light scattered evenly in all directions
// anisotropy = -1 (back-scatter)
// anisotropy = 1: (front scatter)
inline float phaseFunction(float angle, float anisotropy)
{
	if (anisotropy != 0.0f){
		return max( 0.0f, ((1.0f - pow(anisotropy, 2) / (pow((1.0f + pow(anisotropy, 2) - 2.0f * anisotropy * cos(angle)), 1.5f)))));
	}
	else {
		return 1.0f;
	}
};