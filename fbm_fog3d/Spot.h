#pragma once
#include "Light.h"

class Spot :
	public Light
{
private:
	float coneEndAngle;

public:
	float coneAngle;
	float conePenumbraAngle;
	float coneFalloff;

	Spot();
	Spot(float3 origin, float3 direction, float intensity, int decayType, float coneAngle, float conePenumbraAngle, float coneFalloff);
	~Spot();

	float GetRadiance(float3 at);
};

