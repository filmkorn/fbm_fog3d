#include "Spot.h"
#include "Fbm_Math.h"
#include "BlinkTypes.h"


Spot::Spot()
{
}

Spot::Spot(float3 origin,
	float3 direction,
	float intensity,
	int decayType,
	float coneAngle,
	float conePenumbraAngle,
	float coneFalloff) 
	: Light(origin, direction, intensity, decayType) , 
	coneAngle(coneAngle / 2.0f),
	conePenumbraAngle(conePenumbraAngle),
	coneFalloff(coneFalloff),
	coneEndAngle(coneAngle / 2.0f + conePenumbraAngle)
{
}


float Spot::GetRadiance(float3 at){
	float radianceAt;
	float3 lightRay = at - origin;
	float solidangle = angleBetween(lightRay, direction);

	//interpolation between cone Angles:
	radianceAt = intensity * clamp(interpolate2d(float2(min(coneAngle, coneEndAngle), 1.0f), float2(max(coneEndAngle, coneAngle), 0.0f), solidangle * 180.0f / M_PI, 0), 0.0f, 1.0f);
	//cone falloff - Gauss Bell function. Nukes coneFalloff is limited to 128.
	if (coneFalloff > 0.0f) {
		radianceAt *= exp(-1.0f * pow(solidangle, 2.0f) / (2 / coneFalloff));
	}


	// no decay:
	if (decayType == 0){ return radianceAt; }
	//with decay:
	else {
		float distance = fabs(length(lightRay));
		return Decay(radianceAt, distance);
	}
}


Spot::~Spot()
{
}
