#pragma once
#include "BlinkTypes.h"
#include "Axis.h"
#include "Ray.h"
#include "Format.h"

//Should be replaced by DD::Image::CarmeraOp.h
class Camera : 	public Axis
{
public:

	float focal;
	float hAp;
	float vAp;
	float near;
	float far;
	float2 win_translate;
	float2 win_scale;
	float winRoll;
	float focalDist;
	float fstop;
	Format format_;

			
	Camera(	float3 translate = float3(0.0f, 0.0f, 0.0f),
			float3 rotate = float3(0.0f, 0.0f, 0.0f),
			float3 scale = float3(1.0f, 1.0f, 1.0f),
			float uniform_scale = 1.0f,
			float3 skew = float3(0.0f, 0.0f, 0.0f),
			float3 pivot = float3(0.0f, 0.0f, 0.0f),
			float focal = 50.0f,
			float hAp = 24.576f,
			float vAp = 18.672f,
			float near = 0.1f,
			float far = 10000.0f,
			float2 wintranslate = float2(0.0f, 0.0f),
			float2 winScale = float2(1.0f, 1.0f),
			float winRoll = 0.0f,
			float focalDist = 2.0f,
			float fstop = 16.0f
			);
	~Camera();

	void format(Format f) { format_ = f; }
	Ray GetEyeRay( float x, float y);
};

