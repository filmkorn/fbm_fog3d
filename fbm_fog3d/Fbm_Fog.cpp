#include "Fbm_Fog.h"
#include "Fbm_Noise4D.h"
#include "Fbm_Math.h"
#include "BlinkTypes.h"
#include "BlinkMath.h"


Fbm_Fog::Fbm_Fog()
{
}

Fbm_Fog::Fbm_Fog(float octaves,
	float gain,
	float lacunarity,
	float frequency,
	float Z,
	float3 scale,
	float3 offset,
	bool useGround,
	float ground,
	float height,
	int interpolationType,
	float gamma,
	float threshold) :
	octaves(octaves),
	gain(gain),
	lacunarity(lacunarity),
	frequency(frequency * 0.01f),
	Z(Z),
	scale(scale),
	offset(offset),
	useGround(useGround),
	ground(ground),
	height(height),
	interpolationType(interpolationType),
	gamma(gamma),
	threshold(threshold)
{
	Noise = Fbm_Noise4D(octaves, gain, lacunarity, frequency);
}


Fbm_Fog::~Fbm_Fog()
{
}

float Fbm_Fog::GetDensity(float3 samplePos){
	float density = 0.0f;
	float heightFade = 1.0f;

	if (useGround){
		heightFade = clamp(interpolate2d(float2(ground, 1.0f), float2(height, 0.0f), samplePos.y, interpolationType), 0.0f, 1.0f);
	}
	if (heightFade > 0.0){
		density = Noise.GetNoise ((samplePos.x + offset.x) / scale.x, (samplePos.y + offset.y) / scale.y, (samplePos.z + offset.z) / scale.z, Z);
		if (density > threshold){
			density = pow(density, 1 / gamma)* heightFade;
		}
	}

	return density;

}

float Fbm_Fog::HeightFade(float ground,
	float height,
	float y,
	int interpolation){

	return clamp(interpolate2d(float2(ground, 1.0f), float2(height, 0.0f), y, interpolation), 0.0f, 1.0f);
}
