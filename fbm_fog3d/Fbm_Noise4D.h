#pragma once
#include "BlinkTypes.h"


float raw_noise_4d(const float x, const float y, const float z, const float w);


class Fbm_Noise4D
{
private:
	float octaves;
	float gain;
	float lacunarity;
	float frequency;

public:
	Fbm_Noise4D();
	Fbm_Noise4D(float octaves,
		float gain,
		float lacunarity,
		float frequency);


	~Fbm_Noise4D();

	// 4D Multi-octave Simplex noise.
	//
	// For each octave, a higher frequency/lower amplitude function will be added to the original.
	// The higher the persistence [0-1], the more of each succeeding octave will be added.
	float GetNoise(float x, float y, float z, float w);
};