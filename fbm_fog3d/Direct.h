#pragma once
#include "Light.h"

class Direct :
	public Light
{
public:
	Direct();
	Direct(float3 direction, float intensity);
	~Direct();

	Ray GetLightRay(float3 at);
	float GetRadiance(float3 at) {
		return intensity;
	}
};

